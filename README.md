# Formation XSL-T

## Exercices

### 0 - Fichier de travail.

Récupérez les fichiers xml de travail situé dans le répertoire `\xml samples`.
Vous effectuerez les exercices suivants à partir de ces fichiers.

### 1 - Expressions XPath

Le fichier XML est : compositeurs.xml

Trouvez les expressions XPATH

1. Les noms de famille (c-a-d les éléments "famille") de tous les compositeurs(chemin absolu)

2. Les noms de famille de tous les compositeurs, si le noeud actuel est un élément "pays"

3. Les compositeurs nés en 1685

4. Le nom des compositeurs nés en 1685

5. L'année de naissance de Mozart

6. Tous les compositeurs sauf Mozart

7. Tous les compositeurs dont le nom de famille commence avec un 'S'

8. Tous les compositeurs dont le nom de famille ne commence PAS avec un 'S'
	La fonction not() inverse la valeur de son argument
\xsl-samples
9. Le nom de famille des compositeurs qui possèdent un particule

10. Les compositeurs nés après 1900

11. Les prénoms et les dates de naissance des compositeurs(= les noeuds ELEMENT prenom et les noeuds ATTRIBUT naissance).
	Il est possible d'effectuer une union d'expressions XPATH avec l'operateur '|'

12. Les compositeurs dont au moins un de leur noms contient un "v"

### 2 - Execution d'une transformation XSL simple

Récupérer la feuille de style `\xsl-samples\compositeursXMLtoHTML.xslt` et exécutez la sur le fichier xml `compositeurs.xml`.

### 3 - Complexification de la transformation XSL avec XPath

Copiez et modifiez le feuille de style de l'exercice 2 pour afficher
les compositeurs répondants aux critères suivants :

- Le cinquième compositeur de la liste
- Les compositeurs avec particules
- Les compositeurs nés avant 1700
- Les compositeurs avec plusieurs prénoms
- Les compositeurs avec le prénom Richard
- Les compositeurs nés entre 1700 et 1800

### 4 - Tris

Copiez et modifiez le feuille de style de l'exercice 2 pour afficher
les compositeurs triés selon l'ordre suivant :

- Les compositeurs triés par leur nom de famille
- Les compositeurs par ordre chronologique selon leur naissance
- Tri sur plusieures clés: Les compositeurs triés par pays et nom de famille

### 5 - Tests

Todo 

### 6 - Modes

Todo

### 7 - Call templates

Todo

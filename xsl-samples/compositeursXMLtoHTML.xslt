<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/>
	
	<!--******************************************************
	    ** "/" template
	    ***************************************************-->
	<xsl:template match="/">
		<html>
			<head>
				<title>Liste</title>
			</head>

			<body>
			
				<h5>La liste des compositeurs</h5>
				
				<ul>
					<xsl:apply-templates select="compositeurs/compositeur/nom"/>
				</ul>
				
			</body>
		</html>
	</xsl:template>
	
	<!--******************************************************
	    ** "nom" template
	    ***************************************************-->
	<xsl:template match="nom">
		<li>
			<xsl:value-of select="famille"/>
		</li>
	</xsl:template>

	
	
</xsl:stylesheet>